module.exports = {
  parser: 'babel-eslint',

  extends: ['airbnb', 'prettier'],

  env: { 
    browser: true,
    jest: true
  },

  globals: {
    expect: true,
    jest: true
  },

  plugins: ['import', 'json', 'security'],

  rules: {
    'arrow-body-style': 0,
    'no-underscore-dangle': 0,
    'prefer-destructuring': 0,

    'no-restricted-syntax': ['error', 'LabeledStatement', 'WithStatement'],
    'import/no-extraneous-dependencies': [
      'error',
      { devDependencies: true, optionalDependencies: false, peerDependencies: false }
    ],
    'no-plusplus': 0
  }
};
