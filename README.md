# Article Site Web

Web application for a coding exercise. Built with [Svelte](https://svelte.dev), [Sapper](https://sapper.svelte.dev/) and [Marked](https://github.com/markedjs/marked).

- Clone repo
- `npm install`
- Start the companion API
- Start the web app in dev mode: `npm run dev`
- Open http://localhost:4000/

### Features

- Live reloading: edit `src/routes/index.svelte` (or any .svelte file) to see

### Structure

- `/` - root, list of articles
- `/article/view/?id={id}` - preview an article (select an article from the homepage)
- `article/new` - create a new article

### To do / improvements

- Don't save if an article with that title and body already exists (`.find`, return 400)
- add Dom Purify on text input https://github.com/coulby29/DOMPurify
- SSR: https://sapper.svelte.dev/docs#Making_a_component_SSR_compatible
- move article preview to a prefetchable route

